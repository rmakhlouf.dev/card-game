# Card Game

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute
the main method in the `com.games.card.Application` class from your IDE.

Alternatively you can use the Spring Boot Maven plugin like so:

> mvn spring-boot:run

This is a spring boot application without web user interface. The user interact with the application
using the terminal

## How to play

- After starting the application, the player start a new Card Game by writing the command: `start`.
- The player can next ask the app to pick a card hand by typing the command : `hand`.
- The player can continue creating card hands or starting new Game.
- The player can leave the game by typing the command `exit`. The application will be stopped.
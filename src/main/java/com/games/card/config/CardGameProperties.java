package com.games.card.config;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@FieldDefaults(level = PRIVATE)
@ConfigurationProperties(prefix = "card-game")
public class CardGameProperties {

  @NotNull @NotEmpty List<String> cardValues;

  @NotNull @NotEmpty List<String> cardColors;
}

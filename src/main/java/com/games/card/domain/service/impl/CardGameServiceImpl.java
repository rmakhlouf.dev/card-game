package com.games.card.domain.service.impl;

import com.games.card.config.CardGameProperties;
import com.games.card.domain.model.Card;
import com.games.card.domain.model.CardGame;
import com.games.card.domain.model.CardHand;
import com.games.card.domain.service.CardGameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class CardGameServiceImpl implements CardGameService {

  final CardGameProperties cardGameProperties;

  @Override
  public CardGame startNewGame() {
    log.info("Starting new Card Game...");
    return CardGame.builder()
        .cardValues(new HashSet<>(cardGameProperties.getCardValues()))
        .cardColors(new HashSet<>(cardGameProperties.getCardColors()))
        .build();
  }

  @Override
  public CardHand createCardHand(final CardGame cardGame) {
    log.info("Creating new Card Hand...");
    final Random rand = new Random();
    final List<Card> availableCards = new ArrayList<>(cardGame.getCards());
    final List<Card> cards =
        Stream.generate(
                () -> {
                  final int index = rand.nextInt(availableCards.size());
                  final Card card = availableCards.get(index);
                  availableCards.remove(index);
                  return card;
                })
            .limit(CardHand.CARDS_COUNT)
            .collect(Collectors.toList());
    return CardHand.builder().cards(cards).build();
  }
}

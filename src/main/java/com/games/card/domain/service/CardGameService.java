package com.games.card.domain.service;

import com.games.card.domain.model.CardGame;
import com.games.card.domain.model.CardHand;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface CardGameService {

  /**
   * Start a new card game.
   *
   * @return new {@link CardGame} instance.
   */
  CardGame startNewGame();

  /**
   * Create a new card hand from the given card game.
   *
   * @param cardGame card game from where we can create a new card hand.
   * @return new {@link CardHand} instance.
   */
  CardHand createCardHand(@NotNull @Valid CardGame cardGame);
}

package com.games.card.domain.model;

import static org.apache.commons.lang3.Validate.notNull;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;

@Value
public class Card {

  @NotNull @Valid CardValue value;

  @NotNull @Valid CardColor color;

  @Builder(toBuilder = true)
  public Card(@NotNull @Valid final CardValue value, @NotNull @Valid final CardColor color) {
    this.value = notNull(value);
    this.color = notNull(color);
  }
}

package com.games.card.domain.model;

import static org.apache.commons.lang3.Validate.notBlank;
import static org.apache.commons.lang3.Validate.notNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;

@Value
public class CardValue {

  /**
   * Card value.<br>
   * Example: '10', '5', 'As', 'Roi'
   */
  @NotBlank String value;

  /** Card value rank. */
  @NotNull Integer rank;

  @Builder(toBuilder = true)
  public CardValue(@NotBlank final String value, @NotNull final Integer rank) {
    this.value = notBlank(value);
    this.rank = notNull(rank);
  }
}

package com.games.card.domain.model;

import static org.apache.commons.lang3.Validate.notBlank;
import static org.apache.commons.lang3.Validate.notNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;

@Value
public class CardColor {

  /**
   * Card color. <br>
   * Example: 'Carreaux', 'Coeur', 'Pique', 'Trèfle'
   */
  @NotBlank String color;

  /** Card color rank. */
  @NotNull Integer rank;

  @Builder(toBuilder = true)
  public CardColor(@NotBlank final String color, @NotNull final Integer rank) {
    this.color = notBlank(color);
    this.rank = notNull(rank);
  }
}

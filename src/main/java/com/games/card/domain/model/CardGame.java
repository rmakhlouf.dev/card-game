package com.games.card.domain.model;

import static org.apache.commons.lang3.Validate.notEmpty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

/** This is the main object to instantiate everytime we start a new game. */
@Slf4j
@Value
public class CardGame {

  /** The values of cards used by this game instance, randomly ordered */
  @NotEmpty List<String> cardValues;

  /** the colors of cards used by this game instance, randomly ordered */
  @NotEmpty List<String> cardColors;

  /** Game available cards. */
  List<Card> cards;

  @Builder
  public CardGame(@NotEmpty final Set<String> cardValues, @NotEmpty final Set<String> cardColors) {
    notEmpty(cardValues);
    this.cardValues = new ArrayList<>(cardValues);
    log.debug("randomly ordering card values...");
    Collections.shuffle(this.cardValues);

    notEmpty(cardColors);
    this.cardColors = new ArrayList<>(cardColors);
    log.debug("randomly ordering card colors...");
    Collections.shuffle(this.cardColors);

    this.cards = generateGameCards();
  }

  private List<Card> generateGameCards() {
    log.debug("Building the available cards for the current game instance...");
    return generateGameCardValues().stream()
        .flatMap(
            cardValue ->
                generateGameCardColors().stream()
                    .map(cardColor -> Card.builder().color(cardColor).value(cardValue).build()))
        .collect(Collectors.toList());
  }

  private List<CardValue> generateGameCardValues() {
    return IntStream.range(0, cardValues.size())
        .mapToObj(index -> CardValue.builder().value(cardValues.get(index)).rank(index).build())
        .collect(Collectors.toList());
  }

  private List<CardColor> generateGameCardColors() {
    return IntStream.range(0, cardColors.size())
        .mapToObj(index -> CardColor.builder().color(cardColors.get(index)).rank(index).build())
        .collect(Collectors.toList());
  }
}

package com.games.card.domain.model;

import static org.apache.commons.lang3.Validate.notEmpty;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;

@Value
public class CardHand {

  /** CardHand size: Number of cards that should be taken by the player. */
  public static final Integer CARDS_COUNT = 10;

  /**
   * Player card hand: Should be 10 {@link Card} part of a specific {@link CardGame} instance
   * available cards.
   */
  @NotEmpty List<Card> cards;

  @Builder
  public CardHand(final List<Card> cards) {
    this.cards = notEmpty(cards);
    if (!CARDS_COUNT.equals(this.cards.size())) {
      throw new IllegalArgumentException("CardHand should contains exactly 10 cards");
    }
  }

  public List<Card> sortCardsByValue() {
    final ArrayList<Card> ret = new ArrayList<>(this.cards);
    ret.sort(Comparator.comparing(card -> card.getValue().getRank()));
    return ret;
  }

  public List<Card> sortCardsByColor() {
    final ArrayList<Card> ret = new ArrayList<>(this.cards);
    ret.sort(Comparator.comparing(card -> card.getColor().getRank()));
    return ret;
  }
}

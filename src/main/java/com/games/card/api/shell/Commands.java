package com.games.card.api.shell;

import com.games.card.domain.model.Card;
import com.games.card.domain.model.CardGame;
import com.games.card.domain.model.CardHand;
import com.games.card.domain.service.CardGameService;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
@RequiredArgsConstructor
public class Commands {

  final CardGameService cardGameService;

  private CardGame cardGame;

  @ShellMethod("Start a new Card Game session.")
  public List<String> start() {
    cardGame = cardGameService.startNewGame();
    return Arrays.asList(
        String.format(">> Cards values order: %s", cardGame.getCardValues()),
        String.format(">> Cards colors order: %s", cardGame.getCardColors()));
  }

  @ShellMethod("Crate a new Card Hand from the current started Card Game")
  public List<String> hand() {
    final CardHand cardHand = cardGameService.createCardHand(cardGame);
    return Arrays.asList(
        String.format(">> Card Hand cards: %s", displayCardHand(cardHand.getCards())),
        String.format(
            ">> Sorted cards by values: %s", displayCardHand(cardHand.sortCardsByValue())),
        String.format(
            ">> Sorted cards by color: %s", displayCardHand(cardHand.sortCardsByColor())));
  }

  private List<String> displayCardHand(final List<Card> cards) {
    return cards.stream()
        .map(card -> "(" + card.getValue().getValue() + ", " + card.getColor().getColor() + ")")
        .collect(Collectors.toList());
  }
}

package com.games.card.domain.service.impl;

import com.games.card.config.CardGameProperties;
import com.games.card.domain.model.CardGame;
import com.games.card.domain.model.CardHand;
import com.games.card.domain.service.CardGameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class CardGameServiceImplTest {

  @Mock private CardGameProperties cardGameProperties;

  private CardGameService service;

  @BeforeEach
  void setUp() {
    openMocks(this);
    service = new CardGameServiceImpl(cardGameProperties);
  }

  @Test
  void Should_CreateAndReturnNewInstanceOfCardGame_When_StartNewGame() {
    // Given
    when(cardGameProperties.getCardColors())
        .thenReturn(asList("Carreaux", "Coeur", "Pique", "Trèfle"));
    when(cardGameProperties.getCardValues())
        .thenReturn(
            asList("As", "5", "10", "8", "6", "7", "4", "2", "3", "9", "Dame", "Roi", "Valet"));

    // When
    final CardGame cardGame = service.startNewGame();

    // Then
    assertNotNull(cardGame);
    assertNotNull(cardGame.getCards());
    assertEquals(13 * 4, cardGame.getCards().size());
  }

  @Test
  void Should_CreateNewCardHandWithRandomCardsFromTheGivenCardGame_When_createCardHand() {
    // Given
    when(cardGameProperties.getCardColors())
        .thenReturn(asList("Carreaux", "Coeur", "Pique", "Trèfle"));
    when(cardGameProperties.getCardValues())
        .thenReturn(
            asList("As", "5", "10", "8", "6", "7", "4", "2", "3", "9", "Dame", "Roi", "Valet"));

    final CardGame cardGame = service.startNewGame();

    // When
    final CardHand cardHand = service.createCardHand(cardGame);

    // Then
    assertNotNull(cardHand);
    assertNotNull(cardHand.getCards());
    assertEquals(CardHand.CARDS_COUNT, cardHand.getCards().size());
  }
}

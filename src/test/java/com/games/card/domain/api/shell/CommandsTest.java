package com.games.card.domain.api.shell;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.util.ReflectionUtils.findMethod;
import static org.springframework.util.ReflectionUtils.invokeMethod;

import com.games.card.Application;
import com.games.card.api.shell.Commands;
import com.games.card.config.CardGameProperties;
import com.games.card.domain.service.impl.CardGameServiceImpl;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.shell.ConfigurableCommandRegistry;
import org.springframework.shell.MethodTarget;
import org.springframework.shell.standard.StandardMethodTargetRegistrar;
import org.springframework.test.util.ReflectionTestUtils;

class CommandsTest {

  private StandardMethodTargetRegistrar registrar = new StandardMethodTargetRegistrar();
  private ConfigurableCommandRegistry registry = new ConfigurableCommandRegistry();

  @BeforeEach
  void setUp() {
    AnnotationConfigApplicationContext context =
        new AnnotationConfigApplicationContext(Application.class);
    CardGameProperties properties = new CardGameProperties();
    properties.setCardValues(
        asList("As", "5", "10", "8", "6", "7", "4", "2", "3", "9", "Dame", "Roi", "Valet"));
    properties.setCardColors(asList("Carreaux", "Coeur", "Pique", "Trèfle"));
    context.getBeanFactory().registerSingleton("cardGameProperties", properties);
    ReflectionTestUtils.setField(
        context.getBean(CardGameServiceImpl.class), "cardGameProperties", properties);
    registrar.setApplicationContext(context);
    registrar.register(registry);
  }

  @Test
  void Should_StartNewCardGameSessionAndReturnRandomlyOrderedCardsValuesAndColors_When_Start() {
    // Given
    Map<String, MethodTarget> commands = registry.listCommands();

    // When
    MethodTarget methodTarget = commands.get("start");
    final List<String> result =
        (List<String>) invokeMethod(methodTarget.getMethod(), methodTarget.getBean());

    // Then
    assertThat(methodTarget, notNullValue());
    assertThat(methodTarget.getMethod(), is(findMethod(Commands.class, "start")));
    assertThat(methodTarget.getAvailability().isAvailable(), is(true));

    assertEquals(2, result.size());
  }

  @Test
  void Should_PickCardHandFromTheCurrentCardGameAndDisplayItOrderedByValuesAndColor_When_Start() {
    // Given
    Map<String, MethodTarget> commands = registry.listCommands();

    // When
    MethodTarget startMethodTarget = commands.get("start");
    MethodTarget handMethodTarget = commands.get("hand");

    // Start a new Game session and pick a card hand
    invokeMethod(startMethodTarget.getMethod(), startMethodTarget.getBean());
    final List<String> result =
        (List<String>) invokeMethod(handMethodTarget.getMethod(), handMethodTarget.getBean());

    // Then
    assertThat(handMethodTarget, notNullValue());
    assertThat(handMethodTarget.getMethod(), is(findMethod(Commands.class, "hand")));
    assertThat(handMethodTarget.getAvailability().isAvailable(), is(true));

    assertEquals(3, result.size());
  }
}

package com.games.card.domain.model;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.hamcrest.core.IsIterableContaining;
import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.Test;

class CardGameTest {

  @Test
  void
      Should_GenerateTheGameAvailableCardsRandomlyRankedValueAndColors_When_InstantiatingNewCardGameObject() {
    // Given
    Set<String> cardValues = new HashSet<>(asList("As", "5", "10"));
    Set<String> cardColors = new HashSet<>(asList("Carreaux", "Coeur"));

    // When
    final CardGame cardGame = new CardGame(cardValues, cardColors);

    // Then
    assertNotNull(cardGame);
    assertNotNull(cardGame.getCards());
    assertThat(cardGame.getCards(), hasSize(6));
    assertThat(
        cardGame.getCards().stream()
            .map(Card::getValue)
            .map(CardValue::getValue)
            .collect(Collectors.toList()),
        IsIterableContaining.hasItems("As", "5", "10"));
    assertThat(
        cardGame.getCards().stream()
            .map(Card::getColor)
            .map(CardColor::getColor)
            .collect(Collectors.toList()),
        IsIterableContaining.hasItems("Carreaux", "Coeur"));

    assertThat(
        cardGame.getCardValues(), IsNot.not(IsIterableContainingInOrder.contains(cardValues)));
    assertThat(
        cardGame.getCardColors(), IsNot.not(IsIterableContainingInOrder.contains(cardColors)));
  }

  @Test
  void
      Should_Not_CreateNewCardGameInstanceWithEmptyCardValuesOrColors_When_InstantiatingNewCardGameObject() {
    // Given
    Set<String> cardValues = new HashSet<>(asList("As", "5", "10"));
    Set<String> cardColors = new HashSet<>(asList("Carreaux", "Coeur"));

    // When
    assertThrows(IllegalArgumentException.class, () -> new CardGame(emptySet(), cardColors));
    assertThrows(NullPointerException.class, () -> new CardGame(null, cardColors));

    assertThrows(IllegalArgumentException.class, () -> new CardGame(cardValues, emptySet()));
    assertThrows(NullPointerException.class, () -> new CardGame(cardValues, null));

    assertThrows(IllegalArgumentException.class, () -> new CardGame(emptySet(), emptySet()));
    assertThrows(NullPointerException.class, () -> new CardGame(null, null));
  }
}

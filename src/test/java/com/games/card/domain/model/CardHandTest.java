package com.games.card.domain.model;

import static java.util.Collections.emptyList;
import static java.util.Collections.nCopies;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.stream.Collectors;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CardHandTest {

  @Test
  void Should_SuccessfullyCreateCardHand_When_InstantiatingNewHandCardObject() {
    // Given
    final Card card =
        Card.builder()
            .value(CardValue.builder().rank(1).value("10").build())
            .color(CardColor.builder().rank(1).color("Carreaux").build())
            .build();

    // When
    final CardHand cardHand = CardHand.builder().cards(nCopies(10, card)).build();
    assertNotNull(cardHand);
  }

  @Test
  void Should_CreateCardHandComposedByTenCards_When_InstantiatingNewHandCardObject() {
    // Given
    final Card card =
        Card.builder()
            .value(CardValue.builder().rank(1).value("10").build())
            .color(CardColor.builder().rank(1).color("Carreaux").build())
            .build();

    // When
    Assertions.assertThrows(
        IllegalArgumentException.class, () -> CardHand.builder().cards(emptyList()).build());

    Assertions.assertThrows(
        IllegalArgumentException.class, () -> CardHand.builder().cards(nCopies(5, card)).build());

    Assertions.assertThrows(
        IllegalArgumentException.class, () -> CardHand.builder().cards(nCopies(12, card)).build());
  }

  @Test
  void Should_returnSortedCardsByValues_When_sortCardsByValue() {
    // Given
    final List<Card> cards =
        List.of(
            Card.builder()
                .value(CardValue.builder().value("10").rank(5).build())
                .color(CardColor.builder().color("Carreaux").rank(1).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("10").rank(5).build())
                .color(CardColor.builder().color("Pique").rank(4).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("2").rank(1).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("2").rank(1).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("As").rank(2).build())
                .color(CardColor.builder().color("Pique").rank(4).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("Dame").rank(4).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("Roi").rank(3).build())
                .color(CardColor.builder().color("Carreaux").rank(1).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("Valet").rank(6).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("5").rank(8).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("4").rank(11).build())
                .color(CardColor.builder().color("Pique").rank(4).build())
                .build());

    final CardHand cardHand = CardHand.builder().cards(cards).build();

    // When
    final List<Card> result = cardHand.sortCardsByValue();

    // Then
    assertNotNull(result);
    assertThat(
        result.stream().map(Card::getValue).map(CardValue::getValue).collect(Collectors.toList()),
        IsIterableContainingInOrder.contains(
            "2", "2", "As", "Roi", "Dame", "10", "10", "Valet", "5", "4"));
  }

  @Test
  void Should_returnSortedCardsByColors_When_sortCardsByColor() {
    // Given
    final List<Card> cards =
        List.of(
            Card.builder()
                .value(CardValue.builder().value("10").rank(5).build())
                .color(CardColor.builder().color("Carreaux").rank(1).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("10").rank(5).build())
                .color(CardColor.builder().color("Pique").rank(4).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("2").rank(1).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("2").rank(1).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("As").rank(2).build())
                .color(CardColor.builder().color("Pique").rank(4).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("Dame").rank(4).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("Roi").rank(3).build())
                .color(CardColor.builder().color("Carreaux").rank(1).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("Valet").rank(6).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("5").rank(8).build())
                .color(CardColor.builder().color("Coeur").rank(2).build())
                .build(),
            Card.builder()
                .value(CardValue.builder().value("4").rank(11).build())
                .color(CardColor.builder().color("Pique").rank(4).build())
                .build());

    final CardHand cardHand = CardHand.builder().cards(cards).build();

    // When
    final List<Card> result = cardHand.sortCardsByColor();

    // Then
    assertNotNull(result);
    assertThat(
        result.stream().map(Card::getColor).map(CardColor::getColor).collect(Collectors.toList()),
        IsIterableContainingInOrder.contains(
            "Carreaux",
            "Carreaux",
            "Coeur",
            "Coeur",
            "Coeur",
            "Coeur",
            "Coeur",
            "Pique",
            "Pique",
            "Pique"));
  }
}
